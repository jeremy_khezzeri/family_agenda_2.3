package Base_de_Donnee;



import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Evenement_Queries {
 
	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "Family_Agenda.db";

	//****************************Evenement**************************************//
	protected static final String TABLE_Evenement = "table_evenement";
	public static final String COL_ID_evenement = "_id";
	public static final int NUM_COL_ID_evenement = 0;
	public static final String COL_Titre_evenement = "Titre";
	public static final int NUM_COL_Titre_evenement = 1;
	public static final String COL_Date_evenement = "Date";
	public static final int NUM_COL_Date_evenement = 2;
	public static final String COL_Type_evenement = "Type";
	public static final int NUM_COL_Type_evenement = 3;
	
	
	
	//******************************************************************//
	public static SQLiteDatabase bdd;
	private static MaBaseSQLite maBaseSQLite;
	//******************************************************************//
	
	public Evenement_Queries(Context context){
		
		maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
	}
 
	public void open(){
		//on ouvre la BDD en ecriture
		bdd = maBaseSQLite.getWritableDatabase();
	}
 
	public static void close(){
		//on ferme l'acc�s � la BDD
		bdd.close();
	}
 
	public SQLiteDatabase getBDD(){
		return bdd;
	}

	public static long insertEvenement(Evenement e){
		ContentValues values = new ContentValues();
		values.put(COL_Titre_evenement, e.getTitre());
		values.put(COL_Date_evenement, e.getDate());
		values.put(COL_Type_evenement, e.getType());
		return bdd.insert(TABLE_Evenement, null, values);
	}
	
	
	public static int updateEvenement(int id, Evenement e){

		ContentValues values = new ContentValues();
		values.put(COL_Titre_evenement,  e.getTitre());
		values.put(COL_Date_evenement, e.getDate());
		values.put(COL_Date_evenement, e.getType());
		return bdd.update(TABLE_Evenement, values, COL_ID_evenement + " = " +id, null);
	}
	
	
	 
		public int removeEvenementWithID(int id){
			return bdd.delete(TABLE_Evenement, COL_ID_evenement + " = " +id, null);
		}
	 
		public static Cursor getEvenementby_Date(String Date){
			String selectQuery = "SELECT *"  + 
					 " FROM "+ TABLE_Evenement	+" WHERE "+COL_Date_evenement				
					 + " Like "+"'"+Date+"'";

		 Cursor c = bdd.rawQuery(selectQuery, null); 
		 
		return c;
		}
		
		public static Cursor getEvenementbyMonth_Year(int Month,int Year){ 
			
			Cursor c;
			 String selectQuery1 = "SELECT *"  + 
						 " FROM "+ TABLE_Evenement	+" WHERE "+COL_Date_evenement				
						 + " Like "+"'"+"___"+Month+"_"+Year+"'";
			 
			 String selectQuery2 = "SELECT *"  + 
					 " FROM "+ TABLE_Evenement	+" WHERE "+COL_Date_evenement				
					 + " Like "+"'"+"___0"+Month+"_"+Year+"'";
			 
			 if(Month<10)
			 c = bdd.rawQuery(selectQuery2, null); 
			 else
				 c = bdd.rawQuery(selectQuery1, null); 
			 
			return c;
		 
		}
		
				
		
		public Evenement cursorToEvenement(Cursor c,int position){
		
			if (c.getCount() == 0)
				return null;
	 
			c.moveToPosition(position);
		
			Evenement e = new Evenement();
	
			e.setId(c.getInt(NUM_COL_ID_evenement));
			e.setTitre(c.getString(NUM_COL_Titre_evenement));
			e.setDate(c.getString(NUM_COL_Date_evenement));
			e.setType(c.getString(NUM_COL_Type_evenement));
			
			return e;
		}
		
		
		

}
