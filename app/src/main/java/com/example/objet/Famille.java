package com.example.objet;

import java.util.ArrayList;
import java.util.List;

public class Famille {

	private List <MembreFamille> membersf;

	
	public Famille(){
		membersf=new ArrayList<MembreFamille>();
	}
	
	public Famille(List<MembreFamille> mf) {
	
		this.membersf = mf;
	}

	public List<MembreFamille> getMembresf() {
		return membersf;
	}

	public void setMembresf(List<MembreFamille> mf) {
		this.membersf = mf;
	}
	
	
	public void addMember(MembreFamille m ){
		this.membersf.add(m);
	}
	
	public void removeMember(MembreFamille m ){
		this.membersf.remove(m);
	}
	
	public void removeAllElements(){
		for(int i=0;i<=membersf.size()-1;i++)
		{
			removeMember(membersf.get(i));
		}
	}
	
	public MembreFamille getLastElement(){
		return membersf.get(membersf.size()-1);
	}
	
	public int sizeFamille(){
		return this.membersf.size();
	}
}
