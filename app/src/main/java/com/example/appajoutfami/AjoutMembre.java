package com.example.appajoutfami;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.animationsdemo.R;
import com.example.objet.Famille;
import com.example.objet.MembreFamille;
import com.examples.AjoutEvenement;

public class AjoutMembre extends Fragment implements OnItemSelectedListener{
	
	
	private View rootView;
	EditText et_prenom;
	EditText et_nom;
	EditText et_dateNaissance;
	EditText et_etat;
	Button bt_ajMembre;
	Toast toast;
	private String[] etat= {"Mere","Pere","Fils","Fille"};
	Spinner spinnerEtat;
	TextView txt;
	MembreFamille mf;
	String s=" ";
	static final Famille f=new Famille();


	protected View onCreate(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 rootView = inflater.inflate(R.layout.fragment_family_ajout, container, false);
        
        et_prenom=(EditText)rootView.findViewById(R.id.et_prenom); 
        et_nom=(EditText)rootView.findViewById(R.id.et_nom);  
        et_dateNaissance=(EditText)rootView.findViewById(R.id.et_dateNaissance);  
        
        ///spinner////
        
        
        System.out.println(etat.length);
        spinnerEtat = (Spinner) rootView.findViewById(R.id.spinnerEtat);
       ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(getActivity(),
          android.R.layout.simple_spinner_item, etat);
        adapter_state
          .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        

        
        spinnerEtat.setAdapter(adapter_state);
        spinnerEtat.setOnItemSelectedListener(this);
        //spinner

        
        bt_ajMembre = (Button)rootView.findViewById(R.id.bt_validerAjout);  

        bt_ajMembre.setOnClickListener(new View.OnClickListener() {
			      @Override
			      public void onClick(View v) {
			    	  
			    	  mf=new MembreFamille(et_prenom.getText().toString(),et_nom.getText().toString(),et_dateNaissance.getText().toString(),spinnerEtat.getSelectedItem().toString());	
			          f.addMember(mf);
			    	
			          
			         /*   Intent secondeActivite = new Intent(AjoutMembre.this, MainActivity.class);
			        Bundle objetbundle = new Bundle();
			          objetbundle.putString("type",et_etat.getText().toString());
			          secondeActivite.putExtras(objetbundle );
			          
			          secondeActivite.putExtra("NEWMEMBER",mf);
			          startActivity(secondeActivite); */
		    	      
		    	     
			          	Fragment fragment = new FamilyMain();
						FragmentManager fragmentManager = getFragmentManager();
						fragmentManager.beginTransaction()
								.setCustomAnimations(
			                    R.animator.card_flip_right_in, R.animator.card_flip_right_out,
			                    R.animator.card_flip_left_in, R.animator.card_flip_left_out)
								.replace(R.id.frame_container, fragment).commit();
						
			   /*       Toast toast = Toast.makeText(getActivity(),mf.toString(),Toast.LENGTH_SHORT);
			          toast.show();*/
			      }
			});
        
        return rootView;
        
        
    }

	public void AfficherElements(Famille f){
		for(int i=0;i<=f.getMembresf().size()-1;i++)
		{
			 Toast toast = Toast.makeText(getActivity(),f.getMembresf().get(i).toString(),Toast.LENGTH_SHORT);
	          toast.show();
			
		}
	}
	
	
	

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return super.onOptionsItemSelected(item);
	}

	@Override
	  public void onItemSelected(AdapterView<?> parent, View view, int position,
			   long id) {
			 
			 }

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	
	
}
