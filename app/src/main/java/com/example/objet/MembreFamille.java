package com.example.objet;

import java.io.Serializable;
import java.util.Date;

public class MembreFamille implements Serializable {

	private String prenom;
	private String nom;
	//private Date dateDeNaissance;
	private String dateDeNaissance;

	/*private enum etat{
		pere,
		mere,
		fils,
		fille,
		autre;
	}
	*/
	
	private String etat;
	private int image;
	public MembreFamille(){
		
	}
	
	
	public MembreFamille(String prenom, String nom, String dateDeNaissance,
			String etat) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.dateDeNaissance = dateDeNaissance;
		this.etat=etat;
	
	}
	
	
	public MembreFamille(String prenom, String nom, String dateDeNaissance,
			String etat,int image) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.dateDeNaissance = dateDeNaissance;
		this.etat=etat;
		this.image=image;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(String dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	
	public int getImage() {
		return image;
	}


	public void setImage(int image) {
		this.image = image;
	}


	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}
	
	public String toString(){
		return "prenom : "+prenom+"\n nom : "+nom +"\n DateNaissance : "+"\n etat : "+ etat;
	}
	
	

}
