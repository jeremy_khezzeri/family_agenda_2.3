package com.example.appajoutfami;


import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.animationsdemo.R;
import com.example.objet.Famille;
import com.example.objet.MembreFamille;
import com.examples.AjoutEvenement;


public class FamilyMain extends Fragment implements OnClickListener {

	
	private View rootView;
	Button bt;
	LinearLayout layoutBase;
	LinearLayout petitLayout;
	LinearLayout parentLayout;
	LinearLayout childrenLayout;
	LinearLayout.LayoutParams paramsLayout;
	LinearLayout.LayoutParams paramsImag;
	static Famille famille=new Famille();
	MembreFamille mdf;
	Toast toast;

	protected View onCreate(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 rootView = inflater.inflate(R.layout.family_fragment, container, false);
		
		

		layoutBase = (LinearLayout) rootView.findViewById(R.id.baseLayout);
		layoutBase.setOrientation(1);
		layoutBase.setWeightSum(1);

		bt = (Button) rootView.findViewById(R.id.bt);

		parentLayout = new LinearLayout(getActivity());
		parentLayout.setOrientation(LinearLayout.HORIZONTAL);
		childrenLayout = new LinearLayout(getActivity());
		childrenLayout.setOrientation(LinearLayout.HORIZONTAL);
		
		petitLayout=new LinearLayout(getActivity());
		petitLayout.setOrientation(LinearLayout.HORIZONTAL);
		
	
		
		
		//Intent intent = rootView.getIntent();
		/*if ((intent != null)&&(intent.hasExtra("NEWMEMBER"))) {
		
				 mdf = (MembreFamille) intent.getExtras()
						.getSerializable("NEWMEMBER");
					mdf.setImage(chooseImage(mdf.getEtat()));
				  famille.addMember(mdf);
				  getIntent().removeExtra("NEWMEMBER"); 

			if (famille.sizeFamille() == 1) {
				
				paramsLayout= new LinearLayout.LayoutParams(
						500,500);
				paramsLayout.gravity=Gravity.CENTER;
				petitLayout.setLayoutParams(paramsLayout); 
				MembreFamille mf=mdf;
				ajoutImage(mf, new TextView(getActivity()),petitLayout,mf.getPrenom());
				layoutBase.addView(petitLayout);
			}
			
			else if (famille.sizeFamille() == 2)
				 
			{	petitLayout.removeAllViews();
				paramsLayout= new LinearLayout.LayoutParams(
							700,700);
				paramsLayout.gravity=Gravity.CENTER;
				petitLayout.setLayoutParams(paramsLayout); 
				
				
				for(int i=0;i<famille.sizeFamille();i++)
				{
					ajoutImage(famille.getMembresf().get(i), new TextView(
							getActivity()), petitLayout,famille.getMembresf().get(i).getPrenom());
				}

				layoutBase.addView(petitLayout);
				

			}
			
			else {
				  layoutBase.setWeightSum(2);
				  layoutBase.removeAllViews();
				  childrenLayout.removeAllViews();
				  paramsLayout = new LinearLayout.LayoutParams(
				  LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
				  parentLayout.setLayoutParams(paramsLayout);//
				  childrenLayout.setLayoutParams(paramsLayout);

				  
				  for (int i =0;i<2; i++) {
						 ajoutImage(famille.getMembresf().get(i), new TextView(
								 getActivity()), parentLayout,famille.getMembresf().get(i).getPrenom());
						  
				   }
				 
				 for (int i = famille.sizeFamille() - 1; i > 1; i--) {
				 ajoutImage(famille.getMembresf().get(i), new TextView(
						 getActivity()), childrenLayout,famille.getMembresf().get(i).getPrenom());
				  
				 }
				  
				  layoutBase.addView(parentLayout);
				  layoutBase.addView(childrenLayout); }

		}*/
			
/*
 * Evenemenet Ajout
 */
		bt.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Fragment fragment = new AjoutMembre();
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction()
						.setCustomAnimations(
	                    R.animator.card_flip_right_in, R.animator.card_flip_right_out,
	                    R.animator.card_flip_left_in, R.animator.card_flip_left_out)
						.replace(R.id.frame_container, fragment).commit();
				
			}
		});

		return rootView;
		
	}
	

	
	
	public void AfficherElements(Famille f) {
		for (int i = 0; i < f.sizeFamille(); i++) {
			toast = Toast.makeText(getActivity(),f.getMembresf().get(i).toString(),
					Toast.LENGTH_LONG);
			toast.show();
		}
		
		
	}

	public void ajoutImage(MembreFamille membre, TextView txt, LinearLayout l,String text) {
		paramsImag = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, 1f);
		paramsImag.gravity=Gravity.CENTER;
		txt.setBackgroundResource(membre.getImage());
		txt.setLayoutParams(paramsImag);
		txt.setText(text);
		//txt.setTextSize(TRIM_MEMORY_BACKGROUND);
		txt.setGravity(Gravity.CENTER|Gravity.BOTTOM);

		txt.setTextColor(Color.BLACK);
		l.addView(txt);
	}

	public int chooseImage(String s) {

		if (s.equals("Mere")) {
			return R.drawable.mere;
		}

		else if (s.equals("Pere")) {
			return R.drawable.pere;
		}

		if (s.equals("Fille")) {
			return R.drawable.fille;
		}

		if (s.equals("Fils")) {
			return R.drawable.fils;
		}
		return 0;
	}

	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}




	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
}
