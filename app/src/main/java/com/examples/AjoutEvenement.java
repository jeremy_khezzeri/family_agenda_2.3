package com.examples;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import Base_de_Donnee.Evenement;
import Base_de_Donnee.Evenement_Queries;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.android.animationsdemo.R;

public class AjoutEvenement  extends Fragment implements OnClickListener {
	
	
/*Declarations            *********************************************************/	
	private View rootView;
	private EditText Titre ;
	private Button Validation;
    private EditText fromDateEtxt;
    private EditText toDateEtxt;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Spinner spinner;
    
    private Evenement_Queries Table_Evenement ;
    private Evenement EvenementFromBdd= null;
/****************************************************************************/	

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		  
		super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.event_ajout, container, false);
	    
        Evenement_Queries Table_Evenement = new Evenement_Queries(getActivity());
        Table_Evenement.open();
        
        /*******************************************************************
	     * Spinner Multi select
	     */
	    
	    
	    spinner = (Spinner) rootView.findViewById(R.id.spinnerRepasEv);
	    
	    final String[] Liste_Choix = { "Repas", "Rendez vous"};
	    
	    spinner.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, Liste_Choix));
        
        findViewsById();
        setDateTimeField();
        
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        
        return rootView;
               
	}
		
	private void findViewsById() {
		Titre = (EditText) rootView.findViewById(R.id.Event_Title_edittext);
		Validation = (Button) rootView.findViewById(R.id.EventAdd_Val);
		Validation.setOnClickListener(this);
        fromDateEtxt = (EditText) rootView.findViewById(R.id.etxt_fromdate);    
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();
        
      //  toDateEtxt = (EditText) rootView.findViewById(R.id.etxt_todate);
       // toDateEtxt.setInputType(InputType.TYPE_NULL);
    }
 
    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
       // toDateEtxt.setOnClickListener(this);
        
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
 
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }
 
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        
        toDatePickerDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
 
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
              //  toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }
 
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
    
   
    @Override
    public void onClick(View view) {
    	
    	
    	
    	
        if(view == fromDateEtxt) {
            fromDatePickerDialog.show();
        } 
        
       /* else if(view == toDateEtxt) {
            toDatePickerDialog.show();
        }  */
        
        else if (view == Validation)
        {
        	 Evenement e = new Evenement(Titre.getText().toString(),fromDateEtxt.getText().toString(),spinner.getSelectedItem().toString());
	    	  Table_Evenement.insertEvenement(e);    	 	    	  
	    	  Toast.makeText(getActivity(), "Ajout", Toast.LENGTH_SHORT).show();
        }
    }
}
	
	
	
	
	 

