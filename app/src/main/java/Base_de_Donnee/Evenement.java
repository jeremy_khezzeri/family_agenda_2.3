package Base_de_Donnee;

public class Evenement {

		
	private static String Titre;
	private static String Date;
	private static String Type;
		
		

		public Evenement()
		{
			Evenement.id = 0;
			Evenement.Titre = null;
			Evenement.Date = null ;
			Evenement.Type = null ;
			
		}
	 
		public Evenement(String Titre,String Date,String Type)
		{
			
			Evenement.Titre = Titre ; 
			Evenement.Date = Date ;
			Evenement.Type = Type ;
		}
		
		public static String getType() {
			return Type;
		}

		public static void setType(String type) {
			Type = type;
		}

		private static int id;
		public static int getId() {
			return id;
		}

		public static void setId(int id) {
			Evenement.id = id;
		}

		public static String getTitre() {
			return Titre;
		}

		public static void setTitre(String titre) {
			Titre = titre;
		}

		public static String getDate() {
			return Date;
		}

		public static void setDate(String date) {
			Date = date;
		}


		public String toString(){
			return "ID : "+id+"\nTitre : "+Titre+"\nDate de l'evenement: "+ Date;
		}
	
	
}
